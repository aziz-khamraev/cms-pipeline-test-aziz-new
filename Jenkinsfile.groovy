// scripted pipeline
node ('master'){

    def server = Artifactory.server 'jfrog-artifactory-server', credentialsId: 'de8b5ebe-3f72-4155-98fd-2e08b393efce'
    def downloadSpec = """{
     "files": [
        {
        "pattern": "cms-libs/com/virgin/voyages/magnolia/virgin-voyages-swagger/v5.3.2-SNAPSHOT/*.war",
        "target": "${WORKSPACE}/"
            }
        ]
    }"""
    
    stage('pull from artifactory'){
        server.download spec: downloadSpec
    }
    stage('rename artifact'){
        sh 'echo hello'
    }
}